## Julia Jupyter-Notebook

**Julia Jupyter Notebook**

[![codecov](https://codecov.io/gh/LaGuer/Jupyt-Nb/branch/master/graph/badge.svg)](https://codecov.io/gh/LaGuer/Jupyt-Nb)

[![Coverage Status](https://coveralls.io/repos/github/LaGuer/Jupyt-Nb/badge.svg?branch=master)](https://coveralls.io/github/LaGuer/Jupyt-Nb?branch=master)

[![Travis](https://travis-ci.org/LaGuer/Jupyt-Nb.svg?branch=master)](https://travis-ci.org/LaGuer/Jupyt-Nb)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/LaGuer/Jupyt-Nb/master)

[![Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/LaGuer/Jupyt-Nb/blob/master/Julia%20Jupiter-Notebook%20Constants%20in%20Cosmology.ipynb)

[![rXiv](https://img.shields.io/badge/rXiv-1904.0218-orange.svg?style=flat)](https://rxiv.org/abs/1904.0218)

<a href="https://notebooks.azure.com/import/gh/laguer/Jupyt-Nb"><img src="https://notebooks.azure.com/launch.png" /></a>


Visualization
-------------

[![nbviewer](https://img.shields.io/badge/view%20on-nbviewer-brightgreen.svg)](https://nbviewer.jupyter.org/gitlab/LaGuer/Jupyt-Nb/blob/master/Julia%20Jupiter-Notebook%20Constants%20in%20Cosmology.ipynb)

[![GitLab gh-pages](https://img.shields.io/badge/Jupyt-Nb-orange.svg?style=flat)](https://LaGuer.gitlab.io/Jupyt-Nb)

[![Latest PDF](https://img.shields.io/badge/PDF-latest-orange.svg?style=flat)](https://gitlab.com/LaGuer/doc-latex/blob/masterdoc-pdf/publication/main.pdf)

